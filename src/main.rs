use rand::Rng;
use std::fmt::{Display, Result, Formatter};
use std::thread;
use std::time::Duration;

struct Simulation {
    particles: Vec<Particle>,
    board: Board
}

struct Board {
    size: (i32, i32)
}

struct Particle {
    position: (i32, i32),
    velocity: (i32, i32)
}

impl Particle {
    fn new(board: &Board) -> Particle {
        let mut rng = rand::thread_rng();
        Particle {
            position: (rng.gen_range(0..board.size.0), rng.gen_range(1..board.size.1)),
            velocity: (if rng.gen_bool(1.0 / 2.0) { 1 } else { -1 }, if rng.gen_bool(1.0 / 2.0) { 1 } else { -1 })
        }
    }

    fn update(&mut self, board: &Board) {
        self.position = (self.position.0 + self.velocity.0, self.position.1 + self.velocity.1);
        if self.position.0 < 1  || self.position.0 >= board.size.0 {
            self.velocity.0 *= -1;
        }
        if self.position.1 < 1 || self.position.1 >= board.size.1 {
            self.velocity.1 *= -1;
        }
    }
}

impl Board {
    fn new() -> Board {
        Board {
            size: (80, 20)
        }
    }
}

impl Simulation {
    fn new() -> Simulation {
        Simulation {
            particles: vec![],
            board: Board::new()
        }
    }

    fn add(&mut self) {
        self.particles.push(
            Particle::new(&self.board)
        );
    }

    fn tick(&mut self) {
        self.particles.iter_mut().for_each(|particle| {
            particle.update(&self.board);
        });
    }
}

impl Display for Simulation {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        let border = |f: &mut Formatter<'_>, nb| {
            if nb == 1 { write!(f, "┏") } else { write!(f, "┗") };
            for _ in 0..=self.board.size.0 {
                write!(f, "━");
            }
            if nb == 1 { write!(f, "┓") } else { write!(f, "┛") };
            Ok(())
        };
        border(f, 1);
        for i in 0..=self.board.size.1 {
            write!(f, "\n┃");
            for j in 0..=self.board.size.0 {
                if self.particles.iter().any(|x| x.position == (j, i)){
                    write!(f, "🬀");
                } else {
                    write!(f, " ");
                }
            }
            write!(f, "┃");
        }
        write!(f, "\n");
        border(f, 2)
    }
}

fn main() {
    let mut simulation = Simulation::new();
    loop {
        simulation.add();
        println!("{}", simulation);
        let delay = Duration::from_secs_f32(1. / 24.);
        thread::sleep(delay);
        simulation.tick();
        println!("\x1b[{}A", simulation.board.size.1 + 4);
    }
}