# Ideal Gas Law Simulation

This Rust project simulates the Ideal Gas Law, allowing you to modify various parameters and visualize the results in the terminal.

## Introduction

The Ideal Gas Law is a fundamental principle in chemistry and physics, describing the relationship between pressure, volume, and temperature in an ideal gas. This project provides a simulation of the Ideal Gas Law, allowing users to adjust parameters and observe the effects through graphical representations in the terminal.

The Ideal Gas Law equation is:

[ PV = nRT ]

where:
- ( P ) is the pressure of the gas,
- ( V ) is the volume of the gas,
- ( n ) is the amount of substance of the gas,
- ( R ) is the ideal gas constant,
- ( T ) is the temperature of the gas.

## Features

- Simulate the Ideal Gas Law.
- Modify parameters: volume, temperature, and amount of gas.
- Display graphical representations of the results in the terminal.
- Interactive command-line interface.

## Installation

1. Ensure you have Rust installed. If not, you can install it from [rust-lang.org](https://www.rust-lang.org/).
2. Clone this repository:
    ```sh
    git clone git@gitlab.com:ArToXxFR/ideal_gas_law_simulation.git
    ```
3. Navigate to the project directory:
    ```sh
    cd ideal_gas_law_simulation/
    ```
4. Build the project:
    ```sh
    cargo build
    ```

## Usage

To run the simulator, use the following command:
```sh
cargo run
```

## To Do

- [x] Simulate particles
- [ ] Modify Temperature
- [ ] Modify Volume
- [ ] Modify amount of gas